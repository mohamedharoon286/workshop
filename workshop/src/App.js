import React from 'react';
import './App.css';
import HomePage from "./Pages/HomePage/HomePage"
import Header from "./defaultLayout/Header/Header"
import Footer from "./defaultLayout/Footer/Footer"
import { Route, BrowserRouter } from 'react-router-dom';
function App() {
  return (
    <BrowserRouter>
          <Header />
        <div>
        <Route path="/" exact component={HomePage} />
        
      </div>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
