import React from "react";

const Footer = () => {
    const style = {
        fontFamily: 'Roboto Slab', 
        textAlign: "center",
        padding : "20px"
    }
    return (
        <footer style = {style}>
            <p>Copyright Â© Mohamed Ahmed Ali Haroon 2020 </p>
        </footer>
    )
}
export default Footer;