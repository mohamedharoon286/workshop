import React from "react";
import "./Input.css"
const Input = (props) => {
    let OutPut = null;
    switch (props.elementType) {
        case ("input"):{
            OutPut = <input {...props.elementConfig} className = "form-control"  value = {props.value}/>
            break;
        }
        case ("textarea"):{
            OutPut = <textarea {...props.elementConfig} className = "form-control" rows = {7} value = {props.value}/>
            break;
        }
    }
    return (
        <div className="form-group">
            {OutPut}
        </div>
    )
}
export default Input;