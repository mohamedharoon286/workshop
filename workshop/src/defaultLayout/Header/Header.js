import React from "react";
import Routes from "../../Routes";
import { Link } from "react-router-dom";
import "./Header.css"
const Header = () => {
    let HeaderRoutes = (
        Routes.map((element, index) => {
            return (
                <li className="nav-item" key={element.name + index}>
                    <Link className="nav-link" to={element.path && `/${element.path}`}>{element.name}</Link>
                </li>
            )
        })
    )
    return (
        <header className="navbar navbar-expand-lg navbar-dark fixed-top  ">
            <div className='container'>
                <Link className="navbar-brand logo" to="/">Start Reactstrap</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav"> {HeaderRoutes}</ul>
                </div>

            </div>
        </header>
    )
}
export default Header;  