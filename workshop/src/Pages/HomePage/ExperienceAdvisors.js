import React from "react";
import John from "../../Assets/2.jpg"
import Mridul from "../../Assets/4.jpg"
import Milisa from "../../Assets/5.jpg"

import { Button, Row, Col } from "reactstrap";
import { Link } from "react-router-dom"
const ExperienceAdvisors = () => {
    let Advisors = [
        {
            name: "John Babu", 
            dics: "Great explorer of the truth, the master-builder of human happiness.", 
            image: John, 
            jobTitle:"creative Developer", 
        },
        {
            name: "Mridul Druva",
            dics: " Great explorer of the truth, the master-builder of human happiness.",
            jobTitle: "creative Developer",
            image :Mridul,
        },
        {
            name: "Milisa john",
            dics: " Great explorer of the truth, the master-builder of human happiness.",
            jobTitle: "creative Developer",
            image :Milisa, 
        }
    ]
    return (
        <Row className="ExperienceAdvisors container py-4 px-2 mx-auto text-center">
            <h2 className = "subTitle py-2  w-100">Experience Advisors</h2>
            <p className = "text-muted mb-5">
                Able an hope of body. Any nay shyness article matters own removal nothing his forming.
                Gay own additions education satisfied the perpetual. If he cause manor happy.
                Without farther she exposed saw man led. Along on happy could cease green oh.
                </p>
            {Advisors.map((element) => {
                return (
                    <Col xs="12" md="4" lg="4" key = {element.name} className = "ElementContainer">
                        <img src={element.image} alt={element.name + "Image"} title={element.name} />
                        <div className="overLay">
                            <div>
                                <h3> About {element.name}</h3>
                                <p>{element.dics}</p>
                                <Link to = "/" className = "Link">Read More</Link>
                            </div>
                        </div>
                        <div className="TitleInfo">
                            <div>
                                <h4 className="name"> {element.name}</h4>
                                <h5 className = "jobTitle">{element.jobTitle}</h5>
                            </div>
                        </div>
                    </Col>
                )
            })}
        </Row>
    )
}
export default ExperienceAdvisors;