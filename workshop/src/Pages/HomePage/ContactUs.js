import React, { Component } from "react";
import Input from "../../defaultLayout/UI/Inputs/Input"
import { Row, Col, Container, Button } from "reactstrap"
export default class ContactUs extends Component {
    constructor() {
        super();
        this.state = {
            Form : {
                name: {
                    elementType: "input",
                    elementConfig: {
                        type: "text",
                        placeholder : "Your Name * ", 
                    },
                    value : "", 
                },
                phone:  {
                    elementType: "input",
                    elementConfig: {
                        type: "text",
                        placeholder : "Your Phone number * ", 
                    },
                    value : "", 
                },
                email:  {
                    elementType: "input",
                    elementConfig: {
                        type: "email",
                        placeholder : "Your Email * ", 
                    },
                    value : "", 
                }
                ,
                Message:  {
                    elementType: "textarea",
                    elementConfig: {
                        type: "text",
                        placeholder : "Your Message * ", 
                    },
                    value : "", 
                }
            }
        }
    }
    render() {
        const FormElements = [];
        for (let key in this.state.Form) {
            if (key !== "Message") {
                FormElements.push({
                    id: key,
                    config: this.state.Form[key],
                })
            }
        }
        return (
            <div className="contactUs">
                <Container className = "text-center mt-2 ">
                    <h2 className = "font-weight-bold"> Contact Us </h2>
                    <p className = "text-muted mb-5 ">Lorem ipsum dolor sit amet consectetur.</p>
                        <form className="row align-items-center justify-content-center ">
                        {/* <Input elementConfig = "...." elementType = " .... " value = " ..... "/> */}
                        <Col xs="12" sm="12" md="6" lg="6">
                            {FormElements.map((element, index) => {
                                console.log("The Form elements ", element.config )
                                return (
                                    <Input
                                        elementType={element.config.elementType}
                                        elementConfig={element.config.elementConfig}
                                        value={element.config.value}
                                        key={element.id}

                                    />
                                )
                            })}
                        </Col>
                        <Col xs="12" sm='12' md="6" lg="6">
                            <Input
                                elementType={this.state.Form["Message"].elementType}
                                elementConfig = {this.state.Form.Message.elementConfig}    
                                value = {this.state.Form.Message.value}
                            />
                        </Col>
                            <Button color="warning"> Send Message </Button>
                        </form>
                    </Container>
            </div>
        )
    }
}
{/* <Col xs="12" md="6" lg="6">
<Row>
    <Col xs="12" md="12" lg="12">
        <Input inputtype="text" placeholder="Your Name *" required name= "userName"/>
    </Col>
    <Col xs="12" md="12" lg="12">
        <Input inputtype="email" placeholder="Your Email *" required name = "userEmail"/>
    </Col>
    <Col xs="12" md="12" lg="12">
        <Input inputtype="number" placeholder="Your Phone *" required name = "userNumber" />
    </Col>
</Row>
</Col>
<Col xs="12" md="6" lg="6">
<Input inputtype="textarea" placeholder="Your Message *" required name = "userMessage"/>
</Col> */}