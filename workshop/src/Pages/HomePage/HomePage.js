import React, { Component } from "react";
import MainPhoto from "./MainPhoto"
import "./HomePage.css"
import ExperienceAdvisors from "./ExperienceAdvisors"
import ContactUs from "./ContactUs"
export default class  HomePage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name : "this is the State.name of the HomePage Component ",
        }
    }
    render() {
        return (
            <>
                <MainPhoto />
                <ExperienceAdvisors />
                <ContactUs/>
            </>
        )
    }
}