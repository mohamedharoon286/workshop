import React from "react";
import { Button } from "reactstrap";
const MainPhoto = () => {
    return (
        <div className="MainPhotoContainer">
            <div>
                <p className = "welcome">Welcome To Our Studio!</p>
                <h1 className ="title">IT'S NICE TO MEET YOU</h1>
                <Button color = "warning"> Tell Me More</Button>
            </div>
        </div>
    )

}
export default MainPhoto;